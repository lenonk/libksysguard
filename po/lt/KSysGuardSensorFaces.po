# Lithuanian translations for libksysguard package.
# Copyright (C) 2020 This file is copyright:
# This file is distributed under the same license as the libksysguard package.
# Automatically generated, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-05 02:19+0000\n"
"PO-Revision-Date: 2022-12-28 03:10+0200\n"
"Last-Translator: Moo <<>>\n"
"Language-Team: none\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.2.2\n"

#: ConfigAppearance.qml:107
#, kde-format
msgid "Presets:"
msgstr "Išankstinės parinktys:"

#: ConfigAppearance.qml:111
#, kde-format
msgid "Load Preset..."
msgstr "Įkelti išankstinę parinktį..."

#: ConfigAppearance.qml:116
#, kde-format
msgid "Get new presets..."
msgstr "Gauti naujas išankstines parinktis..."

#: ConfigAppearance.qml:128
#, kde-format
msgid "Save Settings As Preset"
msgstr "Įrašyti nuostatas kaip išankstinę parinktį"

#: ConfigAppearance.qml:138
#, kde-format
msgid "Title:"
msgstr "Pavadinimas:"

#: ConfigAppearance.qml:144
#, kde-format
msgid "Show Title"
msgstr "Rodyti pavadinimą"

#: ConfigAppearance.qml:149 facepackages/facegrid/contents/ui/Config.qml:54
#, kde-format
msgid "Display Style:"
msgstr "Rodinio stilius:"

#: ConfigAppearance.qml:169
#, kde-format
msgid "Get New Display Styles..."
msgstr "Gauti naujus rodinio stilius..."

#: ConfigAppearance.qml:179
#, kde-format
msgid "Minimum Time Between Updates:"
msgstr "Mažiausias laikas tarp atnaujinimų:"

#: ConfigAppearance.qml:188
#, kde-format
msgid "No Limit"
msgstr "Neribotas"

#. i18ndp doesn't handle floats :(
#: ConfigAppearance.qml:192
#, kde-format
msgid "1 second"
msgstr "1 sek."

#: ConfigAppearance.qml:194
#, kde-format
msgid "%1 seconds"
msgstr "%1 sek."

#: ConfigSensors.qml:131
#, kde-format
msgid "Total Sensor"
msgid_plural "Total Sensors"
msgstr[0] "Bendras jutiklis"
msgstr[1] "Bendri jutikliai"
msgstr[2] "Bendri jutikliai"
msgstr[3] "Bendri jutikliai"

#: ConfigSensors.qml:150
#, kde-format
msgid "Sensors"
msgstr "Jutikliai"

#: ConfigSensors.qml:176
#, kde-format
msgid "Text-Only Sensors"
msgstr "Tik tekstiniai jutikliai"

#: facepackages/barchart/contents/ui/Config.qml:35
#: facepackages/linechart/contents/ui/Config.qml:47
#: facepackages/piechart/contents/ui/Config.qml:34
#, kde-format
msgid "Show Sensors Legend"
msgstr "Rodyti jutiklių legendą"

#: facepackages/barchart/contents/ui/Config.qml:39
#, kde-format
msgid "Stacked Bars"
msgstr "Sudurtinės juostos"

#: facepackages/barchart/contents/ui/Config.qml:43
#: facepackages/linechart/contents/ui/Config.qml:59
#, kde-format
msgid "Show Grid Lines"
msgstr "Rodyti tinklelio linijas"

#: facepackages/barchart/contents/ui/Config.qml:47
#: facepackages/linechart/contents/ui/Config.qml:63
#, kde-format
msgid "Show Y Axis Labels"
msgstr "Rodyti Y ašies etiketes"

#: facepackages/barchart/contents/ui/Config.qml:51
#: facepackages/piechart/contents/ui/Config.qml:69
#, kde-format
msgid "Automatic Data Range"
msgstr "Automatinis duomenų rėžis"

#: facepackages/barchart/contents/ui/Config.qml:55
#: facepackages/piechart/contents/ui/Config.qml:73
#, kde-format
msgid "From:"
msgstr "Nuo:"

#: facepackages/barchart/contents/ui/Config.qml:62
#: facepackages/piechart/contents/ui/Config.qml:80
#, kde-format
msgid "To:"
msgstr "Iki:"

#: facepackages/colorgrid/contents/ui/Config.qml:25
#, kde-format
msgid "Use sensor color:"
msgstr "Naudoti jutiklio spalvą:"

#: facepackages/colorgrid/contents/ui/Config.qml:30
#: facepackages/facegrid/contents/ui/Config.qml:35
#, kde-format
msgid "Number of Columns:"
msgstr "Stulpelių skaičius:"

#: facepackages/colorgrid/contents/ui/Config.qml:37
#: facepackages/facegrid/contents/ui/Config.qml:42
#, kde-format
msgctxt "@label"
msgid "Automatic"
msgstr "Automatiškai"

#: facepackages/linechart/contents/ui/Config.qml:42
#, kde-format
msgid "Appearance"
msgstr "Išvaizda"

#: facepackages/linechart/contents/ui/Config.qml:51
#, kde-format
msgid "Stacked Charts"
msgstr "Sudurtinės diagramos"

#: facepackages/linechart/contents/ui/Config.qml:55
#, kde-format
msgid "Smooth Lines"
msgstr "Glotnios linijos"

#: facepackages/linechart/contents/ui/Config.qml:67
#, kde-format
msgid "Fill Opacity:"
msgstr "Užpildo nepermatomumas:"

#: facepackages/linechart/contents/ui/Config.qml:73
#, kde-format
msgid "Data Ranges"
msgstr "Duomenų rėžiai"

#: facepackages/linechart/contents/ui/Config.qml:78
#, kde-format
msgid "Automatic Y Data Range"
msgstr "Automatinis Y duomenų rėžis"

#: facepackages/linechart/contents/ui/Config.qml:82
#, kde-format
msgid "From (Y):"
msgstr "Nuo (Y):"

#: facepackages/linechart/contents/ui/Config.qml:89
#, kde-format
msgid "To (Y):"
msgstr "Iki (Y):"

#: facepackages/linechart/contents/ui/Config.qml:99
#, kde-format
msgid "Amount of History to Keep:"
msgstr "Kiek saugoti istorijos:"

#: facepackages/linechart/contents/ui/Config.qml:102
#, kde-format
msgctxt "%1 is seconds of history"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekundę"
msgstr[1] "%1 sekundes"
msgstr[2] "%1 sekundžių"
msgstr[3] "%1 sekundę"

#: facepackages/piechart/contents/ui/Config.qml:38
#, kde-format
msgid "Start from Angle:"
msgstr "Pradėti nuo kampo:"

#: facepackages/piechart/contents/ui/Config.qml:43
#, kde-format
msgctxt "angle degrees"
msgid "%1°"
msgstr "%1°"

#: facepackages/piechart/contents/ui/Config.qml:46
#: facepackages/piechart/contents/ui/Config.qml:59
#, kde-format
msgctxt "angle degrees"
msgid "°"
msgstr "°"

#: facepackages/piechart/contents/ui/Config.qml:51
#, kde-format
msgid "Total Pie Angle:"
msgstr "Bendras skritulio kampas:"

#: facepackages/piechart/contents/ui/Config.qml:56
#, kde-format
msgctxt "angle"
msgid "%1°"
msgstr "%1°"

#: facepackages/piechart/contents/ui/Config.qml:64
#, kde-format
msgid "Rounded Lines"
msgstr "Apvalios linijos"

#: facepackages/textonly/contents/ui/Config.qml:24
#, kde-format
msgid "Group sensors based on the value of the total sensors."
msgstr "Grupuoti jutiklius pagal visų jutiklių reikšmę."

#: import/Choices.qml:53
#, kde-format
msgctxt "@label"
msgid "Click to select a sensor…"
msgstr "Spustelėkite, norėdami pasirinkti jutiklį…"

#: import/Choices.qml:368
#, kde-format
msgid "Search..."
msgstr "Ieškoti..."

#: import/Choices.qml:381
#, kde-format
msgctxt "@action:button"
msgid "Back"
msgstr "Atgal"

#: SensorFaceController.cpp:431
#, kde-format
msgid "System Monitor Sensor"
msgstr "Sistemos prižiūryklės jutiklis"

#~ msgid "User Interface"
#~ msgstr "Naudotojo sąsaja"

#~ msgid ""
#~ "The compact representation of the sensors plasmoid when collapsed, for "
#~ "instance in a panel."
#~ msgstr ""
#~ "Kompaktiškas jutiklių Plasma įskiepio atvaizdavimas, pavyzdžiui, "
#~ "skydelyje."

#~ msgid "The representation of the plasmoid when it's fully expanded."
#~ msgstr "Plasma įskiepio atvaizdavimas, kai jis pilnai išskleistas."

#~ msgid "The optional configuration page for this face."
#~ msgstr "Pasirinktinės konfigūracijos puslapis šiai išvaizdai."

#~ msgid "Configuration support"
#~ msgstr "Konfigūracijos palaikymas"

#~ msgid "KConfigXT xml file for face-specific configuration options."
#~ msgstr ""
#~ "KConfigXT xml failas, skirtas specifinėms išvaizdos konfigūravimo "
#~ "parinktims."

#~ msgid ""
#~ "The configuration file that describes face properties and capabilities."
#~ msgstr "Konfigūracijos failas, aprašantis išvaizdos savybes ir galimybes."
